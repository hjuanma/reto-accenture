﻿using Data;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class CommentController : Controller
    {
        private readonly IComment _IComment;

        public CommentController( IComment _Icomment)
        {
            _IComment = _Icomment;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<Comment> model = new List<Comment>();
            model = _IComment.GetComments;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            Comment model = _IComment.GetComment(id);
            return View(model);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(Comment collection)
        {
            try
            {
                // TODO: Add insert logic here
                var json = string.Format("'postId': '{0}', 'name': {1}, 'email': {2}, 'body': {3}", collection.postId,collection.Name,collection.Email,collection.Body);
                bool response = _IComment.PushComment("comments", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            Comment model = _IComment.GetComment(id);
            return View(model);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Comment collection)
        {
            try
            {
                // TODO: Add update logic here
                var json = string.Format("'id':'{0}', 'postId': '{1}', 'name': '{2}', 'email': '{3}', 'body': '{4}'", collection.Id, collection.postId, collection.Name, collection.Email, collection.Body);
                bool response = _IComment.UpdateComment("comments", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _IComment.DeleteComment("albums", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
