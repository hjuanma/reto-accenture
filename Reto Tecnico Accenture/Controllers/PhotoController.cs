﻿using Data;
using Reto_Tecnico_Accenture.Helper;
using Reto_Tecnico_Accenture.Models;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class PhotoController : Controller
    {
        private readonly IPhoto _IPhoto;

        public PhotoController(IPhoto _Iphoto)
        {
            _IPhoto = _Iphoto;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<Photo> model = new List<Photo>();
            model = _IPhoto.GetPhotos;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            Photo model = _IPhoto.GetPhoto(id);
            var view = ToView(model);
            return View(view);
        }

        // GET: Album/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(PhotoView collection)
        {
            try
            {
                var pic = string.Empty;
                var folder = "~/photos";

                if (collection.FotoFile != null)
                {
                    pic = FileHelper.UploadPhoto(collection.FotoFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var photo = ToPhoto(collection);
                photo.ThumbnailUrl = pic;

                // TODO: Add insert logic here
                var json = string.Format("'albumId': '{0}', 'title': '{1}', 'url': '{2}', 'thumbnailUrl': '{3}'", photo.AlbumId, photo.Title, photo.Url, photo.ThumbnailUrl);
                bool response = _IPhoto.PushPhoto("photos", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            Photo model = _IPhoto.GetPhoto(id);
            var view = ToView(model);
            return View(view);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PhotoView collection)
        {
            try
            {
                // TODO: Add update logic here
                var pic = string.Empty;
                var folder = "~/photos";

                if (collection.FotoFile != null)
                {
                    pic = FileHelper.UploadPhoto(collection.FotoFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var photo = ToPhoto(collection);
                photo.ThumbnailUrl = pic;

                // TODO: Add insert logic here
                var json = string.Format("'id':'{0}','albumId': '{1}', 'title': '{2}', 'url': '{3}', 'thumbnailUrl': '{4}'",photo.Id, photo.AlbumId, photo.Title, photo.Url, photo.ThumbnailUrl);
                bool response = _IPhoto.UpdatePhoto("photos", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {

            Photo model = _IPhoto.GetPhoto(id);
            var view = ToView(model);
            return View(view);
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, PhotoView collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _IPhoto.DeletePhoto("photos", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private Photo ToPhoto(PhotoView view)
        {
            return new Photo
            {
                Id = view.Id,
                AlbumId = view.AlbumId,
                ThumbnailUrl = view.ThumbnailUrl,
                Url = view.Url,
                Title = view.Title,
            };

        }

        private PhotoView ToView(Photo photo)
        {
            return new PhotoView
            {
                Id = photo.Id,
                AlbumId = photo.AlbumId,
                ThumbnailUrl = photo.ThumbnailUrl,
                Url = photo.Url,
                Title = photo.Title,
            };
        }
    }
}
