﻿using Data;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class AlbumController : Controller
    {
        private readonly IAlbum _IAlbum;

        public AlbumController(IAlbum _Ialbum)
        {
            _IAlbum = _Ialbum;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<Album> model = new List<Album>();
            model = _IAlbum.GetAlbums;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            Album model = _IAlbum.GetAlbum(id);
            return View(model);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(Album collection)
        {
            try
            {
                // TODO: Add insert logic here
                var json = string.Format("'title': '{0}', 'userId': '{1}'",collection.Title,collection.UserId);
                bool response = _IAlbum.PushAlbum("albums", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            Album model = _IAlbum.GetAlbum(id);
            return View(model);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Album collection)
        {
            try
            {
                // TODO: Add update logic here
                var json = string.Format("'id': '{0}', 'title': '{1}', 'userId': '{2}'",collection.Id, collection.Title, collection.UserId);
                bool response = _IAlbum.UpdateAlbum("albums",json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _IAlbum.DeleteAlbum("albums", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
