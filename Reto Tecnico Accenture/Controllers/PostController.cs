﻿using Data;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class PostController : Controller
    {
        private readonly IPost _IPost;

        public PostController(IPost _Ipost)
        {
            _IPost = _Ipost;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<Post> model = new List<Post>();
            model = _IPost.GetPosts;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            Post model = _IPost.GetPost(id);
            return View(model);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(Post collection)
        {
            try
            {
                // TODO: Add insert logic here
                var json = string.Format("'userId': '{0}', 'title': '{1}', 'body': '{2}'", collection.UserId, collection.Title, collection.Body);
                bool response = _IPost.PushPost("posts", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            Post model = _IPost.GetPost(id);
            return View(model);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Post collection)
        {
            try
            {
                // TODO: Add update logic here
                var json = string.Format("'id': '{0}','userId': '{1}', 'title': '{2}', 'body': '{3}'",collection.Id, collection.UserId, collection.Title, collection.Body);
                bool response = _IPost.UpdatePost("posts", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _IPost.DeletePost("posts", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
