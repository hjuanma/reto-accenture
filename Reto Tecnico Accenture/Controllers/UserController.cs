﻿using Data;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class UserController : Controller
    {
        private readonly IUser _IUser;

        public UserController(IUser _Iuser)
        {
            _IUser = _Iuser;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<User> model = new List<User>();
            model = _IUser.GetUsers;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            User model = _IUser.GetUser(id);
            return View(model);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(User collection)
        {
            try
            {
                // TODO: Add insert logic here
                var json = string.Format("'name': '{0}', 'username': '{1}', 'email': '{2}', " +
                    "'address': { 'street': '{3}', 'suite': '{4}', 'city': '{5}', 'zipcode': '{6}', " +
                    "'geo': {'lat': '{7}','lng': '{8}'}}," +
                    "'phone': '{9}','website': '{10}','company': {'name': '{11}','catchPhrase': '{12}','bs': '{13}' }", 
                    collection.Name, collection.UserName, collection.Email, collection.Address.Street, collection.Address.Suite, collection.Address.City, collection.Address.Zipcode,
                    collection.Address.Geo.lat, collection.Address.Geo.lng, collection.Phone, collection.Website, collection.Company.Name, collection.Company.CatchPhrase, collection.Company.Bs);
                bool response = _IUser.PushUser("users", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            User model = _IUser.GetUser(id);
            return View(model);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, User collection)
        {
            try
            {
                // TODO: Add update logic here
                var json = string.Format("'id':'{0}','name': '{1}', 'username': '{2}', 'email': '{3}', " +
                    "'address': { 'street': '{4}', 'suite': '{5}', 'city': '{6}', 'zipcode': '{7}', " +
                    "'geo': {'lat': '{8}','lng': '{9}'}}," +
                    "'phone': '{10}','website': '{11}','company': {'name': '{12}','catchPhrase': '{13}','bs': '{14}' }",
                    collection.Id, collection.Name, collection.UserName, collection.Email, collection.Address.Street, collection.Address.Suite, collection.Address.City, collection.Address.Zipcode,
                    collection.Address.Geo.lat, collection.Address.Geo.lng, collection.Phone, collection.Website, collection.Company.Name, collection.Company.CatchPhrase, collection.Company.Bs);

                bool response = _IUser.UpdateUser("users", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _IUser.DeleteUser("users", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
