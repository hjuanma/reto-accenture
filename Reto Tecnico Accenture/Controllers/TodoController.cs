﻿using Data;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture.Controllers
{
    public class TodoController : Controller
    {
        private readonly ITodo _ITodo;

        public TodoController(ITodo _Itodo)
        {
            _ITodo = _Itodo;
        }

        // GET: Album
        public ActionResult Index()
        {
            List<Todo> model = new List<Todo>();
            model = _ITodo.GetTodos;

            return View(model);
        }

        // GET: Album/Details/5
        public ActionResult Details(int id)
        {
            Todo model = _ITodo.GetTodo(id);
            return View(model);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Album/Create
        [HttpPost]
        public ActionResult Create(Todo collection)
        {
            try
            {
                // TODO: Add insert logic here
                var json = string.Format("'userId': '{0}', 'title': '{1}'", collection.UserId, collection.Title);
                bool response = _ITodo.PushTodo("todos", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int id)
        {
            Todo model = _ITodo.GetTodo(id);
            return View(model);
        }

        // POST: Album/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Todo collection)
        {
            try
            {
                // TODO: Add update logic here
                var json = string.Format("'id': '{0}','userId': '{1}', 'title': '{2}'", id, collection.UserId, collection.Title);
                bool response = _ITodo.UpdateTodo("todos", json);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Album/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                bool response = _ITodo.DeleteTodo("todos", id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
