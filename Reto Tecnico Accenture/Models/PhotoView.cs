﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Models
{
    public class PhotoView : Photo
    {
        public HttpPostedFileBase FotoFile { get; set; }
    }
}