﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface IPost
    {
        List<Post> GetPosts { get; }

        Post GetPost(int id);

        bool PushPost(string recurso, string insertion);

        bool UpdatePost(string recurso, string update);

        bool DeletePost(string recurso, int id);
    }

}

