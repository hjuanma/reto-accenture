﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface ITodo
    {
        List<Todo> GetTodos { get; }

        Todo GetTodo(int id);

        bool PushTodo(string recurso, string insertion);

        bool UpdateTodo(string recurso, string update);

        bool DeleteTodo(string recurso, int id);
    }
}
