﻿using Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface IAlbum
    {

        List<Album> GetAlbums { get; }

        Album GetAlbum(int id);

        bool PushAlbum(string recurso, string insertion);

        bool UpdateAlbum(string recurso, string update);

        bool DeleteAlbum(string recurso, int id);

    }
}
