﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface IUser
    {
        List<User> GetUsers { get; }

        User GetUser(int id);

        bool PushUser(string recurso, string insertion);

        bool UpdateUser(string recurso, string update);

        bool DeleteUser(string recurso, int id);
    }
}
