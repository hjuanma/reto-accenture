﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface IComment
    {
        List<Comment> GetComments { get; }

        Comment GetComment(int id);

        bool PushComment(string recurso, string insertion);

        bool UpdateComment(string recurso, string update);

        bool DeleteComment(string recurso, int id);
    }
}
