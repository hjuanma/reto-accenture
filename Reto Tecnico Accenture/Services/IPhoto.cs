﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reto_Tecnico_Accenture.Services
{
    public interface IPhoto
    {
        List<Photo> GetPhotos { get; }

        Photo GetPhoto(int id);

        bool PushPhoto(string recurso, string insertion);

        bool UpdatePhoto(string recurso, string update);

        bool DeletePhoto(string recurso, int id);
    }
}
