﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class TodoRepository : ITodo
    {
        public List<Todo> GetTodos
        {
            get
            {
                List<Todo> todos = new List<Todo>();
                var todoJson = Conection.Get("todos");

                todos = JsonConvert.DeserializeObject<List<Todo>>(todoJson);

                return todos;
            }
        }

        public bool DeleteTodo(string recurso, int id)
        {
            return Conection.Delete(recurso, id);
        }

        public Todo GetTodo(int id)
        {
            Todo todo;
            var postJson = Conection.Get("todos", id);

            todo = JsonConvert.DeserializeObject<Todo>(postJson);

            return todo;
        }

        public bool PushTodo(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);
        }

        public bool UpdateTodo(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);
        }

    }
}