﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class PostRepository : IPost
    {
        public List<Post> GetPosts {
            get
            {
                List<Post> posts = new List<Post>();
                var postJson = Conection.Get("posts");

                posts = JsonConvert.DeserializeObject<List<Post>>(postJson);

                return posts;
            }
        }

        public bool DeletePost(string recurso, int id)
        {
            return Conection.Delete(recurso, id);
        }

        public Post GetPost(int id)
        {
            Post post;
            var postJson = Conection.Get("posts", id);

            post = JsonConvert.DeserializeObject<Post>(postJson);

            return post;
        }

        public bool PushPost(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);
        }

        public bool UpdatePost(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);
        }
    }
}