﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class AlbumRepository : IAlbum
    {
        public List<Album> GetAlbums
        {
            get
            {
                List<Album> albums = new List<Album>();
                var albumJson = Conection.Get("albums");

                albums = JsonConvert.DeserializeObject<List<Album>>(albumJson);

                return albums;
            }
        }

        public Album GetAlbum(int id)
        {
            Album album;
            var albumJson = Conection.Get("albums",id);

            album = JsonConvert.DeserializeObject<Album>(albumJson);

            return album;
 
        }

        public bool PushAlbum(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);
        }

        public bool UpdateAlbum(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);

        }

        public bool DeleteAlbum(string recurso, int id)
        {
            return Conection.Delete(recurso, id);
        }
    }
}