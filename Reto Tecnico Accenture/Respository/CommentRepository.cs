﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class CommentRepository : IComment
    {
        public List<Comment> GetComments
        {
            get
            {
                List<Comment> comments = new List<Comment>();
                var commentJson = Conection.Get("comments");

                comments = JsonConvert.DeserializeObject<List<Comment>>(commentJson);

                return comments;
            }

        }

        public bool DeleteComment(string recurso, int id)
        {
            return Conection.Delete(recurso, id);
        }

        public Comment GetComment(int id)
        {
            Comment comment;
            var commentJson = Conection.Get("comments", id);

            comment = JsonConvert.DeserializeObject<Comment>(commentJson);

            return comment;
        }

        public bool PushComment(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);

        }

        public bool UpdateComment(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);
        }
    }
}