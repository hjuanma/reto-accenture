﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class PhotoRepository : IPhoto
    {
        public List<Photo> GetPhotos {
            get
            {
                List<Photo> photos = new List<Photo>();
                var photosJson = Conection.Get("photos");

                photos = JsonConvert.DeserializeObject<List<Photo>>(photosJson);

                return photos;
            }
        }

        public bool DeletePhoto(string recurso, int id)
        {
            return Conection.Delete(recurso, id);

        }

        public Photo GetPhoto(int id)
        {
            Photo photo;
            var phototJson = Conection.Get("photos", id);

            photo = JsonConvert.DeserializeObject<Photo>(phototJson);

            return photo;
        }

        public bool PushPhoto(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);
        }

        public bool UpdatePhoto(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);
        }
    }
}