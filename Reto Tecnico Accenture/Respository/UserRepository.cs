﻿using Data;
using Newtonsoft.Json;
using Reto_Tecnico_Accenture.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reto_Tecnico_Accenture.Respository
{
    public class UserRepository : IUser
    {
        public List<User> GetUsers
        {
            get
            {
                List<User> users = new List<User>();
                var userJson = Conection.Get("users");

                users = JsonConvert.DeserializeObject<List<User>>(userJson);

                return users;
            }
        }

        public bool DeleteUser(string recurso, int id)
        {
            return Conection.Delete(recurso, id);
        }

        public User GetUser(int id)
        {
            User user;
            var userJson = Conection.Get("users", id);

            user = JsonConvert.DeserializeObject<User>(userJson);

            return user;
        }

        public bool PushUser(string recurso, string insertion)
        {
            return Conection.Push(recurso, insertion);
        }

        public bool UpdateUser(string recurso, string update)
        {
            return Conection.UpDate(recurso, update);
        }
    }
}