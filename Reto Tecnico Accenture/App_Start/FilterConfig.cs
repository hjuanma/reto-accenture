﻿using System.Web;
using System.Web.Mvc;

namespace Reto_Tecnico_Accenture
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
