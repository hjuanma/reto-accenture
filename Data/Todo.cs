﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Todo : Dato
    {
        public int UserId { get; set; }

        public string Title { get; set; }

        public bool Completed { get; set; }
    }
}
