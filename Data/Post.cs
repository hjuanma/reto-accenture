﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Post : Dato
    {
        public int UserId { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

    }
}
