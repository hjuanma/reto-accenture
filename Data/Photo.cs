﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Photo : Dato
    {

        public int AlbumId { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string ThumbnailUrl { get; set; }

    }
}
