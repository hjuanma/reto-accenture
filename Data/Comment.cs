﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Comment : Dato
    {
        public int postId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public String Body { get; set; }
    }
}
