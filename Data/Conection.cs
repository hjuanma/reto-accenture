﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Data
{
    public static class Conection
    {

        private const string URL = "https://jsonplaceholder.typicode.com";


        #region Get
        public static string Get(string recurso)
        {
            var json = new WebClient().DownloadString(string.Format("{0}/{1}", URL, recurso));
            //dynamic m = JsonConvert.DeserializeObject(json);

            return json;
        }

        public static string Get(string recurso, int id)
        {
            var json = new WebClient().DownloadString(string.Format("{0}/{1}/{2}", URL, recurso, id));
            //dynamic m = JsonConvert.DeserializeObject(json);

            return json;

        }

        #endregion
        
        #region Push
        public static bool Push(string recurso, string insertion)
        {

            try
            {
                var NewObject = JObject.Parse(insertion);
                var JsonData = (JObject)NewObject;

                var json = JsonConvert.SerializeObject(JsonData);

                var request = WebRequest.CreateHttp(string.Format("{0}/{1}", URL, recurso));

                request.Method = "POST";
                request.ContentType = "application/json";

                var buffer = Encoding.UTF8.GetBytes(json);
                request.ContentLength = buffer.Length;
                request.GetRequestStream().Write(buffer, 0, buffer.Length);

                var response = request.GetResponse();

                insertion = (new StreamReader(response.GetResponseStream())).ReadToEnd();

                return true;
            }
            catch (Exception)
            {

                return false;

            }
        }
        #endregion

        #region Update

        public static bool UpDate(string recurso, string update)
        {

            try
            {

                JObject NewObject = JObject.Parse(update);
                JObject JsonData = (JObject)NewObject;
                var json = JsonConvert.SerializeObject(JsonData);

                var request = WebRequest.CreateHttp(string.Format("{0}/{1}", URL, recurso));
                request.Method = "PUT";
                request.ContentType = "application/json";

                var buffer = Encoding.UTF8.GetBytes(json);
                request.ContentLength = buffer.Length;
                request.GetRequestStream().Write(buffer, 0, buffer.Length);

                var response = request.GetResponse();
                json = (new StreamReader(response.GetResponseStream())).ReadToEnd();


                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        #endregion

        #region Delete

        public static bool Delete(string recurso, int id)
        {
            try
            {

                var request = WebRequest.CreateHttp(string.Format("{0}/{1}/{2}", URL, recurso, id));

                request.Method = "DELETE";

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        #endregion
    }
}
