﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class User : Dato
    {
        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public Address Address { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }

        public Company Company { get; set; }
    }

    public class Address
    {
        public string Street { get; set; }

        public string Suite { get; set; }

        public string City { get; set; }

        public string Zipcode { get; set; }

        public Geo Geo { get; set; }

    }

    public class Geo
    {
        public double lat { get; set; }

        public double lng { get; set; }
    }

    public class Company
    {
        public string Name { get; set; }

        public string CatchPhrase { get; set; }

        public string Bs { get; set; }
    }
}
